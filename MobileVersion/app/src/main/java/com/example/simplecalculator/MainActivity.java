package com.example.simplecalculator;

import androidx.appcompat.app.AppCompatActivity;

import android.os.Bundle;
import android.text.InputType;
import android.util.JsonWriter;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;
import android.widget.Toast;

import org.json.JSONObject;

public class MainActivity extends AppCompatActivity {

    private double firstNumber;
    private double secondNumber;
    private String sign;
    private double result;
    EditText firstNumb;
    EditText secondNumb;
    EditText sing;
    TextView resultt;

    @Override
    protected void onCreate(Bundle savedInstanceState) {

        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        firstNumb = (EditText) findViewById(R.id.firstNumb);
        firstNumb.setRawInputType(InputType.TYPE_CLASS_NUMBER);

        secondNumb = (EditText) findViewById(R.id.seconNumb);
        secondNumb.setRawInputType(InputType.TYPE_CLASS_NUMBER);

        sing = (EditText) findViewById(R.id.sign);

        Button button = (Button) findViewById(R.id.buttonCalc);
        resultt = (TextView) findViewById(R.id.textView4);

        button.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                firstNumber = Double.valueOf(firstNumb.getText().toString());
                secondNumber = Double.valueOf(secondNumb.getText().toString());
                sign = sing.getText().toString();

                switch (sign) {
                    case ("+"):
                        resultt.setText(plus(firstNumber, secondNumber));
                        break;

                    case ("-"):
                        resultt.setText(minus(firstNumber, secondNumber));
                        break;

                    case ("*"):
                        resultt.setText(multiply(firstNumber, secondNumber));
                        break;

                    case (":"):
                    case ("/"):
                        resultt.setText(divide(firstNumber, secondNumber));
                        break;

                    default:
                        resultt.setText("Wrong sign!");
                }

            }
        });

    }

    public String plus(double a, double b) {

        return Double.toString(a + b);
    }

    public String minus(double a, double b) {

        return Double.toString(a - b);
    }

    public String multiply(double a, double b) {

        return Double.toString(a * b);
    }

    public String divide(double a, double b) {

        if (b == 0) {

            Toast.makeText(getApplicationContext(), "Деление на ноль запрещено!", Toast.LENGTH_SHORT).show();
            return "Введите второй аргумент заново";
        } else {

            return Double.toString(a / b);
        }
    }
}
