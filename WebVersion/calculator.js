    var number1Input = document.getElementById('number1');
    var number2Input = document.getElementById('number2');
    var signInput = document.getElementById('sign');
    var calculateButton = document.getElementById('calcBtn');

    var calculate = function() {
        var value1 =new Number(number1Input.value);
        var value2 =new Number(number2Input.value);
        var sign = signInput.value;
    
        switch (sign) {
          case "+":
            var result = value1+value2;
            document.getElementById('resultField').innerHTML = result;
            break;
          case "-":
            var result = value1-value2;
            document.getElementById('resultField').innerHTML = result;
            break;
          case "*":
           var result = value1*value2;
           document.getElementById('resultField').innerHTML = result;
            break;
          case "/":
             var result = value1/value2;
             if (value2 == 0){

              document.getElementById('resultField').innerHTML = "Don't divide by zero"
             }
             else{
               var result = value1/value2;
               document.getElementById('resultField').innerHTML = result;
             }
             
            break;
           default:
            alert( "wrong sign!" );
        }
        


    }

    calculateButton.addEventListener('click',calculate);

