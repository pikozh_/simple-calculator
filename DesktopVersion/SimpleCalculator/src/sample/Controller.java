package sample;

import javafx.event.ActionEvent;
import javafx.fxml.FXML;
import javafx.scene.control.Button;
import javafx.scene.control.TextField;

public class Controller {

    @FXML
    private TextField firstNumber;

    @FXML
    private TextField secondNumber;

    @FXML
    private TextField sign;

    @FXML
    private Button calculateButton;

    @FXML
    private TextField resultLabel;

    @FXML
    void handleButtonAction(ActionEvent event) {

        resultLabel.setText(calculate(firstNumber, secondNumber, sign));

    }


    public String calculate(TextField firstInputNumber, TextField secondInputNumber, TextField signInput) {
        double a = 0;
        double b = 0;
        try {
            a = Double.parseDouble(firstInputNumber.getText());

        } catch (Exception e) {
            return "First argument is illegal. Please enter numbers";
        }

        try {
            b = Double.parseDouble(secondInputNumber.getText());
        } catch (Exception e) {
            return "Second argument is illegal. Please enter numbers";
        }

        String sign = signInput.getText();
        String result;

        switch (sign) {
            case "+":
                result = Double.toString(a + b);
                break;
            case "-":
                result = Double.toString(a - b);
                break;
            case "/":

                if (b == 0) {
                    result = "You can`t divide by zero!";
                } else {
                    result = Double.toString(a / b);
                }

                break;
            case "*":
                result = Double.toString(a * b);
                break;
            default:
                result = "Введен неправильный знак";
        }

        return result;
    }

}
