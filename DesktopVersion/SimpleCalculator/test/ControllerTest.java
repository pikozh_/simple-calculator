import javafx.scene.control.TextField;
import org.junit.Assert;
import org.junit.Test;
import sample.Controller;



public class ControllerTest {
    Controller controller = new Controller();
    @Test
    public void calculatePass(){
        TextField textFieldFirstInputNumber = new TextField("34");
        TextField textFieldFirstInputNotNumber = new TextField("cwcwcwcca");
        TextField textFieldSecondInputNumber = new TextField("34");
        TextField textFieldInputSign = new TextField("+");

        String actualValue = controller.calculate(textFieldFirstInputNumber,textFieldSecondInputNumber,textFieldInputSign);
        String expectedValueForInputNumber = "68";
        String expectedValueForInputNotNumber = "First argument is illegal. Please enter numbers";

        Assert.assertEquals(expectedValueForInputNumber,actualValue);
        Assert.assertEquals(expectedValueForInputNotNumber,textFieldFirstInputNotNumber);
    }

    @Test
    public void calculateFail(){
        TextField textFieldFirstInputNumber = new TextField("0");
        TextField textFieldSecondInputNumber = new TextField("0");
        TextField textFieldInputSign = new TextField("+");

        String actualValue = controller.calculate(textFieldFirstInputNumber,textFieldSecondInputNumber,textFieldInputSign);
        String expectedValue = "3";

        Assert.assertNotEquals(expectedValue,actualValue);
    }

}
